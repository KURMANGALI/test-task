export interface IJoke {
    id: string
    value: string
    url: string
    isFavorite?: boolean
}